#ifndef SOLOSCURO_UTIL_H
#define SOLOSCURO_UTIL_H

#include <orx.h>

extern orxOBJECT* sol_util_make_object(orxOBJECT *win, const char *name);

#endif
