#include "soloscuro.h"
#include "soloscuro/player.h"
#include "soloscuro/util.h"
#include "soloscuro/window.h"

static orxOBJECT *win;
static int32_t    id;
static orxOBJECT *character, *inv, *spells, *psionics;
static orxOBJECT *pb, *pbl;
static orxOBJECT *game_menu, *game_return;
static orxOBJECT *players[4], *ai[4], *leader[4];
static orxOBJECT *character_ebox;
static orxOBJECT *player_info[4];
static int32_t    last_click = -1;
static int32_t    player_selected = -1;

static int character_click(soloscuro_state_t *state) {
    soloscuro_create_popup_message(state, "char not implemented");
    return EXIT_FAILURE;
}

static int inventory_click(soloscuro_state_t *state) {
    soloscuro_create_popup_message(state, "inventory not implemented");
    return EXIT_FAILURE;
}

static int spells_click(soloscuro_state_t *state) {
    soloscuro_create_popup_message(state, "spells not implemented");
    return EXIT_FAILURE;
}

static int psionics_click(soloscuro_state_t *state) {
    soloscuro_create_popup_message(state, "psionics not implemented");
    return EXIT_FAILURE;
}

// mage/cleric/psionic
static int powers_click(soloscuro_state_t *state) {
    //soloscuro_create_popup_message(state, "powers not implemented");
    return EXIT_FAILURE;
}

static int power_level_click(soloscuro_state_t *state) {
    //soloscuro_create_popup_message(state, "power level not implemented");
    return EXIT_FAILURE;
}

static int game_menu_click(soloscuro_state_t *state) {
    soloscuro_create_popup_message(state, "game menu not implemented");
    return EXIT_FAILURE;
}

static int game_return_click(soloscuro_state_t *state) {
    //soloscuro_create_popup_message(state, "game return not implemented");
    //return EXIT_FAILURE;
    return soloscuro_destroy_top_window(state);
}

static int display_mini_stats(int slot) {
    orxOBJECT *hp, *psi, *state = NULL;
    sol_dude_t *player = NULL;
    sol_player_get(slot, &player);
    char buf[1024];

    printf("[%d]: player: %p\n", slot, player);
    soloscuro_child_rec(player_info[slot], "ViewCharacterHPObject", &hp);
    soloscuro_child_rec(player_info[slot], "ViewCharacterPSIObject", &psi);
    soloscuro_child_rec(player_info[slot], "ViewCharacterStateObject", &state);

    if (player) {
        snprintf(buf, 1023, "%d/%d\n", player->stats.hp, player->stats.high_hp);
    } else {
        strcpy(buf, "");
    }
    printf("hp: %p\n", hp);
    orxObject_SetTextString(hp, buf);

    if (player) {
        snprintf(buf, 1023, "%d/%d\n", player->stats.psp, player->stats.high_psp);
    } else {
        strcpy(buf, "");
    }
    orxObject_SetTextString(psi, buf);

    if (player) {
        snprintf(buf, 1023, "%s\n", "okay");
    } else {
        strcpy(buf, "");
    }
    orxObject_SetTextString(state, buf);

}

static int update_mini_stats() {
    display_mini_stats(0);
    display_mini_stats(1);
    display_mini_stats(2);
    display_mini_stats(3);
}

extern int soloscuro_ds1_view_character_init(soloscuro_state_t *state) {
    orxVECTOR pos;
    soloscuro_get_top_window(state, &win, &id);

    printf("View Character Init: %p, %d\n", win, id);

    // grab all the buttons
    soloscuro_get_button_object(win, id, 1, &character);
    soloscuro_get_button_object(win, id, 2, &inv);
    soloscuro_get_button_object(win, id, 3, &spells);
    soloscuro_get_button_object(win, id, 4, &psionics);
    soloscuro_get_button_object(win, id, 5, &pb);
    soloscuro_get_button_object(win, id, 6, &pbl);
    soloscuro_get_button_object(win, id, 7, &game_menu);
    soloscuro_get_button_object(win, id, 8, &game_return);
    for (int i = 0; i < 4; i++) {
        soloscuro_get_button_object(win, id, 9 + i, &players[i]);
        soloscuro_get_button_object(win, id, 13 + i, &leader[i]);
        soloscuro_get_button_object(win, id, 17 + i, &ai[i]);
    }
    soloscuro_get_ebox_background(win, id, 0, &character_ebox);
    orxObject_GetPosition(character_ebox, &pos);
    pos.fX = 800;
    orxObject_SetPosition(character_ebox, &pos);

    player_info[0] = sol_util_make_object(win, "Player0Status");
    orxObject_GetPosition(player_info[0], &pos);
    pos.fX = 60;
    pos.fY = 65;
    pos.fZ = 0;
    orxObject_SetPosition(player_info[0], &pos);
    player_info[1] = sol_util_make_object(win, "Player1Status");
    orxObject_GetPosition(player_info[1], &pos);
    pos.fX = 110;
    pos.fY = 65;
    pos.fZ = 0;
    orxObject_SetPosition(player_info[1], &pos);
    player_info[2] = sol_util_make_object(win, "Player2Status");
    orxObject_GetPosition(player_info[2], &pos);
    pos.fX = 60;
    pos.fY = 125;
    pos.fZ = 0;
    orxObject_SetPosition(player_info[2], &pos);
    player_info[3] = sol_util_make_object(win, "Player3Status");
    orxObject_GetPosition(player_info[3], &pos);
    pos.fX = 110;
    pos.fY = 125;
    pos.fZ = 0;
    orxObject_SetPosition(player_info[3], &pos);

    update_mini_stats();

    last_click = -1;

    return soloscuro_ds1_view_character_select(state);
}

extern int soloscuro_ds1_view_character_select(soloscuro_state_t *state) {
    //state->gui.ebox = character_ebox;
    state->gui.ebox = NULL;

    for (int i = 0; i < 4; i++) {
        orxObject_SetCurrentAnim(players[i], "Disabled");
        orxObject_SetCurrentAnim(leader[i], "Disabled");
        orxObject_SetCurrentAnim(ai[i], "Disabled");
    }

    // Right now disable power buttons
    orxObject_Enable(pb, 0);
    orxObject_Enable(pbl, 0);

    orxObject_SetTextString(character_ebox, "");
}

static int player_click(soloscuro_state_t *state, int player, int right_click) {
    last_click = 1;// player
    player_selected = player;
    if (right_click) {
        return soloscuro_ds1_popup_create(state, "INACTIVE CHARACTER", "NEW", "ADD", "CANCEL");
    }
}

extern int soloscuro_ds1_view_character_click(soloscuro_state_t *state, int button_num) {
    //printf("button: %d\n", button_num);
    switch(button_num) {
        case 1: return character_click(state);
        case 2: return inventory_click(state);
        case 3: return spells_click(state);
        case 4: return psionics_click(state);
        case 5: return powers_click(state);
        case 6: return power_level_click(state);
        case 7: return game_menu_click(state);
        case 8: return game_return_click(state);
        case 9: case 10: case 11: case 12: return player_click(state, 12 - button_num, orxInput_HasNewStatus("RightClick"));

    }
}

static int player_click_return(soloscuro_state_t *state) {
    switch (state->gui.last_selection) {
        case -1: case 2: goto finish;
        case 0:
            //return soloscuro_create_popup_message(state, "New Char Not implemented");
            last_click = 2; // New Character
            state->gui.last_selection = -1;
            return soloscuro_create_window(state, DS1_WINDOW_NEW_CHARACTER);
        case 1:
            last_click = 3; // ADD char
            return soloscuro_ds1_als_create(state, ADD_WINDOW, player_selected);
    }

finish:
    return EXIT_SUCCESS;
}

extern int soloscuro_ds1_view_character_return(soloscuro_state_t *state) {
    switch (last_click) {
        case 1: return player_click_return(state);
        case 2:
                last_click = -1;
                // TODO: Add Character HERE!
                soloscuro_d1_new_char_get();
                break;
        case 3:
                update_mini_stats();
                break;
    }

    return EXIT_SUCCESS;
}

extern int soloscuro_ds1_view_character_key_down(soloscuro_state_t *state, char c) {
    switch (c) {
        case '1': case '2': case '3': case '4': return player_click(state, c - '1', 1);
            break;
    }
}
