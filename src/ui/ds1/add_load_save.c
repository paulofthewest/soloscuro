// save Heading 2056
// save button: 2057
// load Heading: 6030
// load button: 6031
// add heading: 6036
// add button: 6037
// drop title: 6038
// drop button: 6039
#include <gff/gff.h>
#include <gff/gfftypes.h>

#include "soloscuro.h"
#include "soloscuro/state.h"
#include "soloscuro/player.h"
#include "soloscuro/window.h"

static enum ds1_als_type_e win_type = SAVE_WINDOW;
static orxOBJECT *win;
static int win_id;
static orxOBJECT *title, *button, *button_text, *exit_button, *delete, *up, *down;
static orxOBJECT *selection[10], *selection_text[10];
static orxOBJECT *box;
// Selection, could be characters to add, or game to load/save.
static char **sel = NULL;
static int player_num, sel_pos;

extern int soloscuro_ds1_als_create(soloscuro_state_t *state, enum ds1_als_type_e type, int num) {
    win_type = type;
    player_num = num;
    sel_pos = 0;
    // This will call init.
    soloscuro_create_window(state, DS1_WINDOW_ADD);
    return EXIT_SUCCESS;
}

static orxOBJECT* make_object(orxOBJECT *win, const char *name) {
    orxVECTOR pos;
    orxOBJECT *obj = orxObject_CreateFromConfig(name);

    orxObject_SetParent(obj, win);
    orxObject_GetPosition(obj, &pos);
    orxObject_SetPosition(obj, &pos);

    return obj;
}

static int populate_selections(soloscuro_state_t *state) {
    char **name = sel + sel_pos;
    int pos = 0;

    while (*name && pos < 10) {
        orxObject_SetTextString(selection_text[pos], *name);
        pos++;
        name++;
    }

    while (pos < 10 ) {
        orxObject_SetTextString(selection_text[pos], "");
        pos++;
    }
}

#define CHAR_BUF_MAX (1<<10)

static int populate_add(soloscuro_state_t *state) {
    int32_t num_entries, amt;
    gff_chunk_header_t chunk;
    uint32_t res_ids[128];
    gff_file_t *file;
    char buf[CHAR_BUF_MAX];

    file = state->man->ds1.charsave;
    gff_get_resource_ids(file, GFF_CHAR, res_ids, &num_entries);
    sel = malloc(sizeof(char*) * (num_entries + 1));
    memset(sel, 0x0, sizeof(char*) * (num_entries + 1));

    //printf("POPULATE ADD: %d\n", num_entries);
    for (uint32_t i = 0; i < num_entries; i++) {
        gff_find_chunk_header(file, &chunk, GFF_CHAR, res_ids[i]);
        amt = gff_read_chunk(file, &chunk, &buf, CHAR_BUF_MAX);
        ds1_combat_t *combat = (ds1_combat_t*)(buf + 10);
        sel[i] = strdup(combat->name);
        printf("%d: len: %d, name:%s\n", i, amt, combat->name);
    }

    populate_selections(state);
}

static int free_sels() {
    if (!sel) { return EXIT_FAILURE; }

    char **name = sel;
    while (*name) {
        free(*name);
        name++;
    }

    free(sel);
    sel = NULL;
}

extern int soloscuro_ds1_als_init(soloscuro_state_t *state) {
    orxVECTOR pos;

    free_sels();

    soloscuro_get_top_window(state, &win, &win_id);
    soloscuro_get_button_object(win, win_id, 0, &title);
    soloscuro_get_button_object(win, win_id, 1, &button);
    soloscuro_get_button_object(win, win_id, 2, &exit_button);
    soloscuro_get_button_object(win, win_id, 3, &delete);
    soloscuro_get_button_object(win, win_id, 14, &up);
    soloscuro_get_button_object(win, win_id, 15, &down);
    soloscuro_get_button_text_object(win, win_id, 1, &button_text);

    for (int i = 4; i <= 13; i++) {
        soloscuro_get_button_object(win, win_id, i, &selection[i - 4]);
        orxObject_GetPosition(selection[i-4], &pos);
        soloscuro_get_button_text_object(win, win_id, i, &selection_text[i - 4]);
        pos.fX += 5;
        orxObject_SetPosition(selection_text[i-4], &pos);
    }
    soloscuro_get_ebox_object(win, win_id, 0, &box);

    orxObject_GetPosition(button, &pos);
    switch(win_type) {
        case ADD_WINDOW:
            //orxObject_Delete(button);
            orxObject_Enable(button_text, 0);
            button = make_object(win, "AddButton");
            //button = make_object(win, "Window3024Button20Object");
            break;
    }
    orxObject_SetPosition(button, &pos);

    switch (win_type) {
        case ADD_WINDOW:
            populate_add(state);
            break;
    }
}

static int selection_pressed(int val) {
    for (int i = 0; i < 10; i++) {
        orxObject_SetCurrentAnim(selection[i], "Inactive");
    }
    orxObject_SetCurrentAnim(selection[val], "PressedHeld");
}

static int find_selected() {
    for (int i = 0; i < 10; i++) {
        if (!strcmp(orxObject_GetCurrentAnim(selection[i]), "PressedHeld")) {
            return i;
        }
    }

    return -1;
}

static int destroy_window(soloscuro_state_t *state) {
    orxObject_Delete(button);
    return soloscuro_destroy_top_window(state);
}

static int handle_als_button(soloscuro_state_t *state) {
    gff_chunk_header_t chunk;
    int32_t num_entries, sel;
    uint32_t res_ids[128];

    if ((sel = find_selected()) < 0) {
        return soloscuro_create_popup_message(state, "Nothing selected.");
    }
    sel += sel_pos;

    switch (win_type) {
        case ADD_WINDOW:
            gff_get_resource_ids(state->man->ds1.charsave, GFF_CHAR, res_ids, &num_entries);
            if (sel < 0 || sel >= num_entries) {
                soloscuro_create_popup_message(state, "NOT VALID");
                goto oob_error;
            }
            if (sol_player_load(state, player_num, res_ids[sel_pos])) {
                soloscuro_create_popup_message(state, "Could not load player.");
                goto load_error;
            }
            return destroy_window(state);
    }

    return EXIT_SUCCESS;
load_error:
oob_error:
    return EXIT_FAILURE;
}

extern int soloscuro_ds1_als_click(void *state, orxOBJECT *btn, int button_num) {
    int32_t num_entries;
    uint32_t res_ids[128];

    if (btn == button) {
        return handle_als_button(state);
    }

    switch (button_num) {
        case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
            return selection_pressed(button_num - 4);
        case 14: // UP
            if (sel_pos > 0) {
                sel_pos--;
            }
            return populate_selections(state);
        case 15: // DOWN
            if (strcmp(orxObject_GetTextString(selection_text[9]), "")) {
                sel_pos++;
            }
            return populate_selections(state);
    }
}
extern int soloscuro_ds1_als_key_down(soloscuro_state_t *state, char c) {
    switch (c) {
        case '0': case '1': case'2': case'3': case'4': case '5': case'6': case'7': case '8':case'9':
            return selection_pressed(c - '0');
        case 10:
            return handle_als_button(state);
    }
    printf("keyboard button: %c\n", c);
}
