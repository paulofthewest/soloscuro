#include <string.h>
#include <stdlib.h>
#include <gff/gff.h>
#include <gff/gfftypes.h>
#include <gff/object.h>

#include "soloscuro.h"
#include "soloscuro/debug.h"
#include "soloscuro/entity.h"
#include "soloscuro/player.h"

static sol_dude_t *players[MAX_PCS] = {NULL, NULL, NULL, NULL};
static int ai[MAX_PCS] = {0, 0, 0, 0};
static int active = -1;
static int count = 0;
static int direction = 0x0;

#define BUF_MAX (1<<12)

static void free_sprites(const int slot) {
}

static int load_character_sprite(const int slot, const float zoom) {
    if (slot < 0 || slot >= MAX_PCS) { return EXIT_SUCCESS; }
    gff_palette_t *pal = open_files[RESOURCE_GFF_INDEX].pals->palettes + 0;
    sol_dude_t *dude;
    sol_player_get(slot, &dude);
    if (!dude) { return EXIT_FAILURE; }

    free_sprites(slot);

    switch(dude->race) {
        case RACE_HALFELF:
            break;
        case RACE_HUMAN:
            break;
        case RACE_DWARF:
            break;
        case RACE_ELF:
            break;
        case RACE_HALFGIANT:
            break;
        case RACE_HALFLING:
            break;
        case RACE_MUL:
            break;
        case RACE_THRIKREEN:
            break;
        default:
            fprintf(stderr, "UNKNOWN RACE!\n");
            exit(1);
            break;
    }
    return EXIT_SUCCESS;
}

extern int sol_player_cleanup() {
    //sol_region_manager_remove_players();
    for (int i = 0; i < MAX_PCS; i++) {
        sol_player_free(i);
    }
    return EXIT_SUCCESS;
}

extern int sol_player_free(const int slot) {
    if (players[slot]) {
        sol_entity_free(players[slot]);
    }
    players[slot] = NULL;
    return EXIT_SUCCESS;
}

extern int sol_player_init() {
    // Setup the slots for reading/writing
    for (int i = 0; i < MAX_PCS; i++) {
        if (!players[i]) {
            sol_player_get(i, &players[i]);
        }
    }
    return EXIT_SUCCESS;
}

extern int sol_player_get(const int slot, sol_entity_t **e) {
    if (slot < 0 || slot >= MAX_PCS) { return EXIT_FAILURE; }
    *e = players[slot];
    //printf("player[%d] = %p\n", slot, players[slot]);
    return EXIT_SUCCESS;
}

extern int sol_player_set(const int slot, sol_entity_t *dude) {
    if (slot < 0 || slot >= MAX_PCS || !dude) { return EXIT_FAILURE; }

    players[slot] = dude;

    if (!players[slot] && active == slot) {
        active = -1;
        for (int i = 0; i < MAX_PCS; i++) {
            if (players[i]) { active = i; return EXIT_SUCCESS; }
        }
    }

    if (active < 0 && players[slot]) {
        sol_player_set_active(slot);
    }
    return EXIT_SUCCESS;
}

extern int sol_player_exists(const int slot) {
    if (slot < 0 || slot >= MAX_PCS) { return EXIT_FAILURE; }
    sol_dude_t *player;
    sol_player_get(slot, &player);
    return (player && (player->name != NULL)) ? EXIT_SUCCESS : EXIT_FAILURE;
}

extern int sol_player_set_active(const int slot) {
    int prev = active;
    if (slot < 0 || slot >= MAX_PCS) { return EXIT_FAILURE; }
    if (players[slot]) { active = slot; }
    return EXIT_SUCCESS;
}

extern int sol_player_get_active(sol_entity_t **e) {
    if (!e) { return EXIT_FAILURE; }
    return sol_player_get(active, e);
}

extern int sol_player_get_slot(sol_entity_t *entity, int *slot) {
    if (!entity || !slot) { return EXIT_FAILURE; }

    *slot = -1;

    for (int i = 0; *slot == -1 && i < MAX_PCS; i++) {
        if (entity == players[i]) { *slot = i; }
    }

    return EXIT_SUCCESS;
}

extern int sol_player_ai(const int slot) {
    if (slot < 0 || slot >= MAX_PCS) { return EXIT_FAILURE; }
    return ai[slot] ? EXIT_SUCCESS : EXIT_FAILURE;
}

extern int sol_player_set_ai(const int slot, const int _ai) {
    if (slot < 0 || slot >= MAX_PCS) { return EXIT_FAILURE; }
    ai[slot] = _ai;
    return EXIT_SUCCESS;
}

extern int sol_player_close() {
    for (int i = 0; i < MAX_PCS; i++) {
        free_sprites(i);
    }

    sol_player_cleanup();
    return EXIT_SUCCESS;
}

static int game_over() {
    sol_entity_t *dude;

    for (int i = 0; i < MAX_PCS; i++) {
        sol_player_get(i, &dude);
        if (dude && dude->stats.hp > 0) { return 0; }
    }

    return 1;
}

extern int sol_player_freeze() {
    return EXIT_FAILURE;
}

extern int sol_player_update() {
    sol_entity_t *dude = NULL;
    int       xdiff = 0, ydiff = 0;
    const int speed = 2;

    sol_player_get_active(&dude);
    //if (game_over()) { sol_map_game_over(); return EXIT_FAILURE; }

    if (--count > 0) { return EXIT_FAILURE; }

    if (direction & PLAYER_UP)    { ydiff -= 1; }
    if (direction & PLAYER_DOWN)  { ydiff += 1; }
    if (direction & PLAYER_LEFT)  { xdiff -= 1; }
    if (direction & PLAYER_RIGHT) { xdiff += 1; }

    if (sol_player_freeze() == EXIT_SUCCESS) {
        xdiff = ydiff = 0;
    }

    //if (sol_narrate_is_open() != EXIT_SUCCESS) {
        //sol_trigger_noorders(dude->mapx, dude->mapy);
        //sol_trigger_los_check();
    //}

    if (xdiff != 0 || ydiff != 0) {
        //sol_trigger_box_check(dude->mapx, dude->mapy);
        //sol_trigger_tile_check(dude->mapx, dude->mapy);

        sol_entity_attempt_move(dude, xdiff, ydiff, speed);
    }

    //count = settings_ticks_per_move() / speed;
    return EXIT_SUCCESS;
}

extern int sol_player_move(const uint8_t _direction) {
    direction |= _direction;
    return EXIT_SUCCESS;
}

extern int sol_player_unmove(const uint8_t _direction) {
    direction &= ~(_direction);
    return EXIT_SUCCESS;
}

extern int sol_player_condense() {
    for (int i = 0; i < MAX_PCS; i++) {
        sol_entity_t *player, *active;
        sol_player_get(i, &player);
        sol_player_get_active(&active);
    }

    return EXIT_SUCCESS;
}

extern int sol_player_get_inventory(const int slot, sol_inventory_t **i) {
    if (!i) { return EXIT_FAILURE; }
    if (!players[slot]->inv) { sol_inventory_create(&players[slot]->inv); }
    *i = (sol_inventory_t*)players[slot]->inv;
    return EXIT_SUCCESS;
}

extern int sol_player_load(soloscuro_state_t *state, const int player, const int res_id) {
    char buf[BUF_MAX];
    gff_rdff_header_t *rdff;
    size_t offset = 0;
    sol_dude_t *dude = NULL, *tmp = NULL;
    gff_chunk_header_t chunk;
    
    gff_find_chunk_header(state->man->ds1.charsave, &chunk, GFF_CHAR, res_id);
    if (gff_read_chunk(state->man->ds1.charsave, &chunk, &buf, sizeof(buf)) < 34) { return 0; }

    sol_player_free(player);
    sol_player_get(player, &dude);
    if (dude) { sol_entity_free (dude); }
    sol_entity_create_fake(30, 10, &tmp);
    sol_player_set(player, tmp);
    sol_player_get(player, &dude);
    sol_entity_load_from_gff(state, dude, player, res_id);

    // The rest appears to be integrated with sol_entity_load_From_gff.
    /*
    rdff = (gff_rdff_header_t*) (buf);
    offset += sizeof(gff_rdff_header_t);
    if (rdff->type == GFF_COMBAT_OBJECT) {
        warn("Combat object encountered in save. Ignoring.\n");
        offset += rdff->len;

        rdff = (gff_rdff_header_t*) (buf + offset);
        offset += sizeof(gff_rdff_header_t);
        offset += rdff->len;
    } else if (rdff->type == GFF_PLAYER_OBJECT) {
        sol_player_free(player);
        sol_player_get(player, &dude);
        printf("dude = %p\n", dude);
        sol_entity_load_from_object(dude, buf + offset);
        //sol_combat_get_scmd(COMBAT_SCMD_STAND_DOWN, &dude->anim.scmd);
        offset += rdff->len;
    }

    rdff = (gff_rdff_header_t*) (buf + offset);
    
    if (rdff->type == GFF_ENTITY_NAME) {
        offset += sizeof(gff_rdff_header_t);
        strncpy(dude->name, buf + offset, 32);
        offset += rdff->len;
    }

    gff_find_chunk_header(state->man->ds1.charsave, &chunk, GFF_PSIN, res_id);
    //if (!gff_read_chunk(id, &chunk, ds_player_get_psi(player), sizeof(psin_t))) { return 0; }

    gff_find_chunk_header(state->man->ds1.charsave, &chunk, GFF_SPST, res_id);
    //if (!gff_read_chunk(id, &chunk, ds_player_get_spells(player), sizeof(ssi_spell_list_t))) { return 0;}

    gff_find_chunk_header(state->man->ds1.charsave, &chunk, GFF_PSST, res_id);
    //if (!gff_read_chunk(id, &chunk, ds_player_get_psionics(player), sizeof(psionic_list_t))) { return 0;}
    */

    return EXIT_SUCCESS;
}
